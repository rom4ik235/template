// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../pages/stores/api.dart' as _i4;
import '../pages/stores/tab1_store/tab1_store.dart' as _i3;
import '../pages/stores/tab2_store/tab2_store.dart'
    as _i5; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// an extension to register the provided dependencies inside of [GetIt]
extension GetItInjectableX on _i1.GetIt {
  /// initializes the registration of provided dependencies inside of [GetIt]
  _i1.GetIt init(
      {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
    final gh = _i2.GetItHelper(this, environment, environmentFilter);
    gh.factory<_i3.Tab1Store>(() => _i3.Tab1Store(get<_i4.ApiProvider>()));
    gh.factory<_i5.Tab2Store>(() => _i5.Tab2Store(get<_i4.ApiProvider>()));
    gh.singleton<_i4.ApiProvider>(_i4.ApiProvider());
    return this;
  }
}
