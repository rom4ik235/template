import 'package:template/pages/main/tab1/main/tab1_page.dart';
import 'package:template/pages/main/tab1/second_tab1_page/second_tab1_page.dart';
import 'package:template/pages/main/tab2/main/tab2_page.dart';
import 'package:template/pages/main/tab2/second_tab2_page/second_tab2_page.dart';
import 'package:template/pages/main/tab_bar_navigator.dart';
import 'package:template/pages/onbording/onbording_page.dart';
import 'package:template/pages/stores/api.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

String isFindStore<T extends StoreName>(BuildContext context) {
  try {
    return "find ${context.read<T>().name}";
  } catch (e) {
    return "Not find $T";
  }
}

Map<String, Widget Function(BuildContext)> routes = <String, WidgetBuilder>{
  OnbordingPage.routeName: (context) => OnbordingPage(),
  TabBarNavigator.routeName: (context) => TabBarNavigator(),
  Tab1Page.routeName: (context) => Tab1Page()
};

Route<dynamic>? generateRouteTab(RouteSettings settings) {
  switch (settings.name) {
    case OnbordingPage.routeName:
      return MaterialPageRoute(builder: (context) => OnbordingPage());
    case SecondTab1Page.routeName:
      return MaterialPageRoute(builder: (context) => SecondTab1Page());
    case TabBarNavigator.routeName:
      return MaterialPageRoute(builder: (context) => TabBarNavigator());
  }
}

Route<dynamic>? generateRouteTab1(RouteSettings settings) {
  switch (settings.name) {
    case Tab1Page.routeName:
    case "/":
      return MaterialPageRoute(builder: (context) => Tab1Page());
  }
}

Route<dynamic>? generateRouteTab2(RouteSettings settings) {
  switch (settings.name) {
    case SecondTab2Page.routeName:
      return MaterialPageRoute(builder: (context) => SecondTab2Page());
    case Tab2Page.routeName:
    case "/":
      return MaterialPageRoute(builder: (context) => Tab2Page());
  }
}

// class Provider extends StatelessWidget {
//   final Widget child;
//   final void Function() create;
//   const Provider({required this.child, required this.create, Key? key})
//       : super(key: key);

//   @override
//   Widget build(BuildContext context) => child;
// }
