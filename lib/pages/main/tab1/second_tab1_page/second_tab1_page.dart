import 'package:template/pages/stores/api.dart';
import 'package:template/pages/stores/tab1_store/tab1_store.dart';
import 'package:template/pages/stores/tab2_store/tab2_store.dart';
import 'package:template/routes.dart';
import 'package:flutter/material.dart';

class SecondTab1Page extends StatelessWidget {
  static const routeName = "/SecondTab1";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(SecondTab1Page.routeName)),
      body: Center(
        child: Column(
          children: [
            Text(isFindStore<Tab1Store>(context)),
            Text(isFindStore<Tab2Store>(context)),
            Text(isFindStore<ApiProvider>(context)),
          ],
        ),
      ),
    );
  }
}
