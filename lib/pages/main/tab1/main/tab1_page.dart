import 'package:template/pages/main/tab1/second_tab1_page/second_tab1_page.dart';
import 'package:template/pages/stores/api.dart';
import 'package:template/pages/stores/tab1_store/tab1_store.dart';
import 'package:template/pages/stores/tab2_store/tab2_store.dart';
import 'package:template/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

class Tab1Page extends StatelessWidget {
  static const routeName = "/Tab1";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(Tab1Page.routeName)),
      body: Center(
        child: Column(
          children: [
            Text(isFindStore<Tab1Store>(context)),
            Text(isFindStore<Tab2Store>(context)),
            Text(isFindStore<ApiProvider>(context)),
            TextButton(
              onPressed: () =>
                  Navigator.pushNamed(context, SecondTab1Page.routeName),
              child: Text("Next"),
            ),
          ],
        ),
      ),
    );
  }
}
