import 'package:template/di/injector.dart';
import 'package:template/pages/stores/tab1_store/tab1_store.dart';
import 'package:template/pages/stores/tab2_store/tab2_store.dart';
import 'package:template/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TabBarNavigator extends StatelessWidget {
  static const String routeName = '/TabBar';
  final controller = CupertinoTabController();
  final List<GlobalKey<NavigatorState>> _keys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];
  TabBarNavigator({Key? key}) : super(key: key);

  BuildContext? get activeContext => _keys[controller.index].currentContext;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (Navigator.canPop(activeContext ?? context)) {
          Navigator.pop(activeContext ?? context);
        }
        return false;
      },
      child: CupertinoTabScaffold(
        controller: controller,
        tabBar: CupertinoTabBar(items: const [
          BottomNavigationBarItem(icon: Icon(Icons.account_tree)),
          BottomNavigationBarItem(icon: Icon(Icons.wallet_travel)),
        ]),
        tabBuilder: (context, index) {
          switch (index) {
            case 0:
              return Provider(
                create: (context) => getIt.get<Tab1Store>(),
                child: CupertinoTabView(
                  onGenerateRoute: generateRouteTab1,
                  onUnknownRoute: generateRouteTab,
                  navigatorKey: _keys[index],
                ),
              );
            default:
              return Provider(
                create: (context) => getIt.get<Tab2Store>(),
                child: CupertinoTabView(
                  onGenerateRoute: generateRouteTab2,
                  onUnknownRoute: generateRouteTab,
                  navigatorKey: _keys[index],
                ),
              );
          }
        },
      ),
    );
  }
}
