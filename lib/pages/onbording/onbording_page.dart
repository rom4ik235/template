import 'package:template/pages/main/tab_bar_navigator.dart';
import 'package:template/pages/stores/api.dart';
import 'package:template/pages/stores/tab1_store/tab1_store.dart';
import 'package:template/pages/stores/tab2_store/tab2_store.dart';
import 'package:template/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

class OnbordingPage extends StatelessWidget {
  static const routeName = "/Onbording";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(OnbordingPage.routeName)),
      body: Center(
        child: Column(
          children: [
            Text(isFindStore<Tab1Store>(context)),
            Text(isFindStore<Tab2Store>(context)),
            Text(isFindStore<ApiProvider>(context)),
            TextButton(
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  TabBarNavigator.routeName,
                  (_) => false,
                );
              },
              child: Text("Login"),
            ),
          ],
        ),
      ),
    );
  }
}
