import 'dart:math';

import 'package:injectable/injectable.dart';

var rng = Random();

@singleton
class ApiProvider extends StoreName {
  ApiProvider() : super("ApiProvider");
}

abstract class StoreName extends Object {
  final String name;
  StoreName(String name) : name = "$name${rng.nextInt(1000)}";
}
