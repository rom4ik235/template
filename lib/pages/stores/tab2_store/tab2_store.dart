import 'package:template/pages/stores/api.dart';
import 'package:injectable/injectable.dart';

@injectable
class Tab2Store extends StoreName {
  final ApiProvider api;
  Tab2Store(this.api) : super("Tab2Store");
}
