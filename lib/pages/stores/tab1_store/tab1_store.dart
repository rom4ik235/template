import 'package:template/pages/stores/api.dart';
import 'package:injectable/injectable.dart';

@injectable
class Tab1Store extends StoreName {
  final ApiProvider api;
  Tab1Store(this.api) : super("Tab1Store");
}
