import 'package:template/di/injector.dart';
import 'package:template/pages/onbording/onbording_page.dart';
import 'package:template/pages/stores/api.dart';
import 'package:template/routes.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  injectDependencies(env: kDebugMode ? Environment.test : Environment.prod);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (context) => getIt.get<ApiProvider>(),
      child: const MaterialApp(
        initialRoute: OnbordingPage.routeName,
        onGenerateRoute: generateRouteTab,
      ),
    );
  }
}
